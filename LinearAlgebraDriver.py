import LinearDependence as ld
import numpy as np

Vector_Space_Dimension = input("Enter dimension of your Real Vectors space: ") #number of columns
Number_Of_Vectors = input("Enter number of vectors: ") #number of rows

rows = int(Number_Of_Vectors)
cols = int(Vector_Space_Dimension)


ListOfVectors = np.zeros(shape=(rows,cols))

for i in range(rows):
    print("Enter vector#", i + 1, ":")
    List = [] # the individual vector the user inputted 
    for j in range(cols):
        print("Enter x",j+1, ":")
        List.append(float(input()))

    vector = np.array(List)
    ld.AddVectorToMatrix(ListOfVectors,vector,i) #add vector to matrix
print("The original matrix: ")
print(ListOfVectors)
    
if(ld.TooManyVectors(cols,rows)):
       print("Linearly Dependent because there are more vectors than the dimension of the Vector Space.")
       exit()
    

for x in ListOfVectors:
    if ld.isZero(x):
        print("Zero Vector is one of the vectors. Therefore your list of vectors is linearly dependent.")
        exit()

ld.RREF(ListOfVectors,0,rows-1)
print("Final matrix: ")
print(ListOfVectors)
print("The list of vectors are linearly independent")
