from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def home_view(request, *args, **kwargs):
	return render(request, "home.html", {})


def about_view(request, *args, **kwargs):
	my_context = {"my_text": "this is text", "number": 3141, "my_list": [1,0,1,6]
	}
	return render(request, "about.html", my_context)