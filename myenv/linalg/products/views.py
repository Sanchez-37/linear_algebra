from django.shortcuts import render

from .forms import ProductForm, RawProductForm
from .models import Product
# Create your views here.
def product_create_view(request):
	form = ProductForm(request.POST or None)
	if form.is_valid():
		form.save()
		form = ProductForm()

	context = {"form": form}
	return render(request, "product/product_create.html", context)

# def product_create_view(request):
# 	# if statement is here so that a None is not printed in the backend
# 	if request.method == "POST":
# 		my_new_title = request.POST.get('title')
# 		print(my_new_title)
# 	context = {}
# 	return render(request, "product/product_create.html", context)

# def product_create_view(request):

# 	my_form = RawProductForm()
# 	if request.method == "POST":
# 		my_form = RawProductForm(request.POST)
# 		if my_form.is_valid():
# 			# now the data ig good
# 			print(my_form.cleaned_data)
# 			Product.objects.create(**my_form.cleaned_data)

# 	context = {"form": my_form}
# 	return render(request, "product/product_create.html", context)

def product_detail_view(request):
	obj = Product.objects.get(id=1)
	# context = {
	# 	'title': obj.title, 'description': obj.description, 'price': obj.price,
	# }

	context = {"object": obj}
	return render(request, "product/detail.html", context)