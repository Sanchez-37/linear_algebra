from django.apps import AppConfig


class LinIndependenceConfig(AppConfig):
    name = 'lin_independence'
