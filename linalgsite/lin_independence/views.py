from django.shortcuts import render
from django.http import HttpResponse, HttpRequest
from django.http import HttpResponseRedirect
from django.template import loader
from django.forms import formset_factory
from .forms import VectorForm
from .models import FrontPageModel
from .models import FrontPageModelForm
from .linear_independence import *
import numpy as np
# Create your views here.


def index(request):
    return render(request, 'lin_independence/front_page.html')

# create context with the values from vector space and number of vectors received from user (as a range)
# the reason for this is looping in the template where the text input matrix will be generated.
def generate_matrix(request):
	# save the value of 'vspace' and 'numvectors' to the database
	f = FrontPageModelForm(request.POST)
	new_input = f.save()

	vspace = int(request.POST['vspace'])
	vspace = range(0,vspace)
	num_vectors = int(request.POST['numvectors'])
	num_vectors = range(0, num_vectors)
	
	context = {"vector": num_vectors, "vspace": vspace}
	# res = "vspace: " + str(vspace) + " numvectors: " + str(num_vectors)

	return render(request, 'lin_independence/input_matrix.html', context=context)

def generate_matrix_two(request):
	if request.method == 'POST':
		if request.POST.get('vspace') and request.POST.get('numvectors'):
			user_input = FrontPageModel()
			user_input.vector_space = request.POST.get('vspace')
			user_input.num_vectors = request.POST.get('numvectors')
			user_input.save()

			vspace = int(request.POST['vspace'])
			num_vectors = int(request.POST['numvectors'])

			numForms = vspace * num_vectors
			formset = formset_factory(VectorForm, extra=numForms)

			return render(request, 'lin_independence/input_matrix2.html', {'formset': formset})
	else:
		pass

def determination(request):

	# grab the newest values of numvectors and vspace from the database
	form_data = FrontPageModel.objects.last()
	print(form_data)
	rows = int(form_data.numvectors)
	cols = int(form_data.vspace)

	ListOfVectors = np.zeros(shape=(rows,cols))

	for i in range(rows):
		List = [] # the individual vector the user inputted 
		for j in range(cols):
			List.append(request.POST[f"v_{i}_{j}"])

		vector = np.array(List)
		AddVectorToMatrix(ListOfVectors,vector,i) #add vector to matrix
	print("The original matrix: ")
	print(ListOfVectors)
    
	if(TooManyVectors(cols,rows)):
		return HttpResponse("Linearly Dependent because there are more vectors than the dimension of the Vector Space.")
		
	for x in ListOfVectors:
		if isZero(x):
			return HttpResponse("Zero Vector is one of the vectors. Therefore your list of vectors is linearly dependent.")


	RREF(ListOfVectors,0,rows-1)
	print(ListOfVectors)
	if AZeroVectorResults(ListOfVectors,0,rows):
		return HttpResponse("A zero vector is produced. Thus vectors are linearly dependent")

	return HttpResponse("vectors are linearly independent")
	# print("Final matrix: ")
	# print(ListOfVectors)
	# print("The list of vectors are linearly independent")

	# something = request.POST['fname_0_0']
	# somethingtwo = request.POST['fname_0_1']


	# return render(request, 'lin_independence/result.html', context=context)