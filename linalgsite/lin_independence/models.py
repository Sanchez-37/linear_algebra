from django.db import models
from django.forms import ModelForm

# Create your models here.

class FrontPageModel(models.Model):
    vspace = models.IntegerField()
    numvectors = models.IntegerField()

class FrontPageModelForm(ModelForm):
    class Meta: 
        model = FrontPageModel
        fields = {'vspace', 'numvectors'}