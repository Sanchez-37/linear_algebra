from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	# url('lin_independence', views.index),
    path('front_page.html/', views.index, name='index'),
	path('generate_matrix/', views.generate_matrix, name='generate_matrix')    
    # path('name.html', views.index, name='get_name')
]