import numpy as np


#determination of whether one of the vectors in the list is the zero vector
def isZero(vector):
    numZeros = 0
    for x in vector:
        if(x == 0):
            numZeros= numZeros + 1
    if numZeros == len(vector):
        return True

    return False

def TooManyVectors(numOfVectors, DimOfVectorSpace):
    return numOfVectors > DimOfVectorSpace

#add vectors as columns of a matrix
def AddVectorToMatrix(Matrix, Vector,row):
    Matrix[row] = Vector

#do reduced row echelon form starting at 'start' row and ending at 'end' row
def RREF(Matrix,start,end):
    #done with RREF
    if start >= end :
        return
    
    #check to see if there is a leading number. If the leading number is a zero then find the next row with a 
    #leading number not equal to zero and replace it with the current row.
    if(Matrix[start][start] == 0):
        row = FindRow(Matrix,start + 1, end)
        swap(Matrix,start,row) #swap the rows
    
        
    #if the leading value is not zero then divide the entire row by the leading value    
    if(Matrix[start][start] != 1):
        DivideRow(Matrix,start)

    SubtractFromRows(Matrix,start + 1,end)

    # if AZeroVectorResults(Matrix,start + 1,end):
    #     print("Final matrix: ")
    #     print(Matrix)
    #     print("Vectors are linearly dependent")
    #     exit()
        

    RREF(Matrix,start + 1,end)
    

#to search for a row with a leading number that is not zero 
# return -1 if no row has a leading non-zero       
def FindRow(Matrix,start, end):
    #we will search a maximum of end - start times
    for i in range(end - start + 1):
        
        if Matrix[start + i][start-1] != 0 :
            return start + i #the desired row is row #(start + i)
    return -1


#procedure to swap rows
def swap(Matrix,start,row):
    Matrix[[start,row]] = Matrix[[row,start]]

#divide the row by the leading value. 'start' is the index of the row you want to divide
def DivideRow(Matrix,start):
    value = Matrix[start][start] #the leading value
    row = Matrix[start,:] #the row you want to divide
    for i in range(len(row)):
        Matrix[start][i] = Matrix[start][i] / value
         #row[i] = [ mrx / float(value) for mrx in row[i]]

#subtract the other rows by the values of the row indexed by start
def SubtractFromRows(Matrix,start,end):
    row = Matrix[start-1,:]
    for i in range(end-start + 1):
        leadingVal = Matrix[start + i][start-1]
        if leadingVal != 0:
            for j in range(len(row)):
                Matrix[start+ i][j] = Matrix[start + i][j] - leadingVal * row[j]
    #M[i] = [ iv - lv*rv for rv,iv in zip(M[r],M[i])]
            
def AZeroVectorResults(Matrix,start,end):
    for i in range(end - start):
        if(isZero(Matrix[start + i,:])):
            return True
    return False
    
    
    







    
        
    
